// 11ty configuration
module.exports = config => {
  config.addWatchTarget("./src/css/")
  config.addWatchTarget("./src/images/")
  config.addPassthroughCopy("./src/css/")
  config.addPassthroughCopy("./src/images/")
  // 11ty defaults
  return {
    dir: {
      input: 'src',
      output: 'public'
    },
    markdownTemplateEngine: "njk"
  };
};

